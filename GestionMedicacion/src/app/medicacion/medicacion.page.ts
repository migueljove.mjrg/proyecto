import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FirestoreService } from '../providers/firestore.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Medicacion } from '../providers/medicacion';
import { AlertController, Platform } from '@ionic/angular';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { CimaRestApiService } from '../providers/cima-rest-api.service';
import { File } from '@ionic-native/File/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';

@Component({
  selector: 'app-medicacion',
  templateUrl: './medicacion.page.html',
  styleUrls: ['./medicacion.page.scss'],
})
export class MedicacionPage implements OnInit {

  id: string;
  medication: Medicacion;
  edit: boolean;

  public medicationForm: FormGroup;

  constructor(private route: ActivatedRoute,
    private firestore: FirestoreService,
    public formBuilder: FormBuilder,
    private location: Location,
    private localNotifications: LocalNotifications,
    public alertController: AlertController,
    private cima: CimaRestApiService,
    private platform: Platform,
    private file: File,
    private ft: FileTransfer,
    private fileOpener: FileOpener,
    private document: DocumentViewer,
  ) {
    this.edit = false;
    this.medicationForm = this.createMyForm();
  }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.firestore.getMedications().subscribe(resp => {
      for (let index = 0; index < resp.length; index++) {
        const element = resp[index];
        if (element.codigo_nacional === this.id) {
          this.medication = element;
          break;
        }
      }
    });

  }
  editar() {
    this.edit = true;
    this.medicationForm.setValue({
      dosis: this.medication.dosis,
      medida: this.medication.medida,
      tiempo_tomas: this.medication.tiempo_tomas,
      duracion_tratamiento: this.medication.duracion_tratamiento,
      comentario: this.medication.comentario,
    });
  }

  borrar() {
    this.presentDeleteConfirm();
  }

  async presentDeleteConfirm() {
    const alert = await this.alertController.create({
      header: 'Borrado',
      message: '¿Esta usted seguro de que quiere eliminar esta medicación?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            this.firestore.deleteMedicacion(this.medication);
            this.localNotifications.cancel(this.medication.codigo_nacional);
            this.location.back();
          }
        }
      ]
    });

    await alert.present();
  }

  private createMyForm() {
    const group: FormGroup = new FormGroup({
      dosis: new FormControl('', Validators.required),
      medida: new FormControl('', Validators.required),
      tiempo_tomas: new FormControl('', Validators.required),
      duracion_tratamiento: new FormControl('', Validators.required),
      comentario: new FormControl('')
    });
    return group;
  }

  updateMedicacion() {
    const dosis = this.medicationForm.controls['dosis'].value;
    const medida = this.medicationForm.controls['medida'].value;
    const tiempo_tomas = this.medicationForm.controls['tiempo_tomas'].value;
    const duracion_tratamiento = this.medicationForm.controls['duracion_tratamiento'].value;
    const comentario = this.medicationForm.controls['comentario'].value;
    const med = new Medicacion(this.medication.name, this.medication.codigo_nacional, dosis, medida, tiempo_tomas, duracion_tratamiento, comentario);
    this.medication = med;
    this.firestore.addMedicacion(med);
    this.noEdit();
  }

  getProspecto() {
    this.cima.getInfo(this.medication.codigo_nacional).subscribe((resp:any) => {
      const url = resp.docs[1].url;
      let path = this.file.dataDirectory;
      const transfer = this.ft.create();
      const pdfName = `${this.medication.codigo_nacional}.pdf`;
      transfer.download(url, path + pdfName).then(entry => {
        let url = entry.toURL();

        if (this.platform.is('ios')) {
          this.document.viewDocument(url, 'application/pdf', {});
        } else {
          this.fileOpener.open(url, 'application/pdf')
            .then(() => console.log('File is opened'))
            .catch(e => console.log('Error opening file', e));
        }
      });
    });
  }

  noEdit() {
    this.edit = false;
  }
  goBack() {
    this.location.back();
  }
}
