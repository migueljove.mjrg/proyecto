import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicacionPage } from './medicacion.page';

describe('MedicacionPage', () => {
  let component: MedicacionPage;
  let fixture: ComponentFixture<MedicacionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicacionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicacionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
