import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Validators, FormBuilder } from '@angular/forms';
import { AlertController } from '@ionic/angular';
import { Events } from '@ionic/angular';
import { Loader } from '../providers/loader';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFirestore } from 'angularfire2/firestore';
import { User } from '../providers/user';
import { Medicacion } from '../providers/medicacion';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})

export class RegistrationPage implements OnInit {
  user: any;

  constructor(
    private events: Events,
    public navCtrl: NavController,
    public db: AngularFireDatabase,
    public firestore: AngularFirestore,
    public afAuth: AngularFireAuth,
    private formBuilder: FormBuilder,
    private loader: Loader,
    private alertCtrl: AlertController
  ) {
    this.user = formBuilder.group({
      fullName: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      passwordConfirmation: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
    });
  }

  ngOnInit() {
  }
  goToLogin() {

  }
  // Create user using form builder controls
  createUser() {
    const fullName = this.user.controls.fullName.value;
    const email = this.user.controls.email.value;
    const password = this.user.controls.password.value;
    const passwordConfirmation = this.user.controls.passwordConfirmation.value;
    this.loader.show('Creating user...');

    new Promise((resolve, reject) => {
      if (passwordConfirmation !== password) {
        reject(new Error('Password does not match'));
      } else {
        resolve();
      }
    })
      .then(() => {
        return this.afAuth.auth.createUserWithEmailAndPassword(email, password);
      })
      .then((user: any) => {
        this.events.publish('user:create', user);
        // Login if successfuly creates a user
        return this.afAuth.auth.signInWithEmailAndPassword(email, password);
      })
      .then((authData: any) => {
        const user = new User(authData.user);
        this.firestore.collection('users').doc(user.uid).set(Object.assign({}, user));
        this.loader.hide();
      })
      .catch((e) => {
        this.loader.hide();
        this.alertCtrl.create({
          header: 'Error',
          message: `Failed to login. ${e.message}`,
          buttons: [{ text: 'Ok' }]
        }).then(alert => alert.present());
      });
  }
}
