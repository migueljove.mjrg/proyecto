import { Component, ChangeDetectorRef } from '@angular/core';
import { FirestoreService } from '../providers/firestore.service';
import { LocalNotifications, ILocalNotification } from '@ionic-native/local-notifications/ngx';
import { AuthenticatorService } from '../providers/authenticator';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import * as firebase from 'firebase';
import { StorageService } from '../providers/storage.service';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Storage } from '@ionic/storage';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { File, FileEntry } from '@ionic-native/File/ngx';
import { HttpClient } from '@angular/common/http';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/Camera/ngx';
import { ActionSheetController, ToastController, Platform, LoadingController } from '@ionic/angular';
import { FilePath } from '@ionic-native/file-path/ngx';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  userData: any;
  scheduled = [];

  constructor(
    private auth: AuthenticatorService,
    private firestore: FirestoreService,
    private localNotifications: LocalNotifications,
    private imagePicker: ImagePicker,
    private cropService: Crop,
    private storageService: StorageService,
    private storage: Storage,
    private filechooser: FileChooser,
    private camera: Camera,
    private file: File,
    private http: HttpClient,
    private webview: WebView,
    private actionSheetController: ActionSheetController,
    private toastController: ToastController,
    private plt: Platform,
    private loadingController: LoadingController,
    private ref: ChangeDetectorRef,
    private filePath: FilePath
  ) { }
  ngOnInit(): void {
    this.getAll();
    this.firestore.getUserData().subscribe(data => {
      this.userData = data;
      this.getImage();
    });
  }
  logOut() {
    // this.localNotifications.cancelAll();
    this.storage.set('uid', 'NoUID');
    this.auth.logOut();
  }

  deleteAllMedications() {
    this.firestore.getMedications().subscribe((resp) => {
      resp.forEach(medication => {
        this.firestore.deleteMedicacion(medication);
      });
      this.localNotifications.cancelAll();
    });
  }

  deleteAccount() {
    this.auth.deleteAccount();
  }
  getAll() {
    this.localNotifications.getAll().then((res: ILocalNotification[]) => {
      this.scheduled = res;
    })
  }

  choose() {
    this.filechooser.open().then((uri) => {
      console.log(uri);
      this.uploadImageToFirebase(uri);
      // this.file.resolveLocalFilesystemUrl(uri).then((newUrl) => {
      //   console.log(newUrl);
      //   let dirPath = newUrl.nativeURL;
      //   console.log(dirPath);
      //   let dirPathElements = dirPath.split('/');
      //   dirPathElements.pop();
      //   dirPath = dirPathElements.join('/');
      //   console.log(dirPath);
      //   this.uploadImageToFirebase(uri);
      // this.file.readAsArrayBuffer(dirPath, newUrl.name).then(async (buffer) => {
      //   await this.upload(buffer).then((resp) => {
      //     this.getImage();
      //   });
      // }).catch((error) => {
      //   console.log("ERROR: " + error);
      // });
      // });
    });
  }

  async upload(buffer) {
    let blob = new Blob([buffer], { type: "image/jpeg" });
    let storageRef = firebase.storage().ref().child('images').child(this.auth.uid);
    console.log(storageRef);
    storageRef.put(blob).then((resp) => {
      console.log(resp);
    });
  }

  openImagePickerCrop() {
    this.imagePicker.hasReadPermission().then(
      (result) => {
        if (result == false) {
          this.imagePicker.requestReadPermission();
        }
        else if (result == true) {
          this.imagePicker.getPictures({
            maximumImagesCount: 1
          }).then(
            (results) => {
              for (var i = 0; i < results.length; i++) {
                this.cropService.crop(results[i], { quality: 75 }).then(
                  newImage => {
                    this.uploadImageToFirebase(newImage);
                  },
                  error => console.error("Error cropping image", error)
                );
              }
            }, (err) => console.log(err)
          );
        }
      }, (err) => {
        console.log(err);
      });
  }

  uploadImageToFirebase(image) {
    // image = this.webView.convertFileSrc(image);

    this.storageService.uploadImage(image)
      .then(() => {
        this.getImage();
      })
  }

  getImage() {
    let storageRef = firebase.storage().ref();
    storageRef.child('images').child(this.auth.uid).getDownloadURL().then((url) => {
      let element = document.getElementById('myImg').setAttribute('src', url);
    }).catch((error) => {
      console.log(error);
    });
  }


  fileChanged(event) {
    if (event.target.files.length > 0) {
      let file = event.srcElement.files[0];
      let storageRef = firebase.storage().ref().child('images').child(this.auth.uid);
      storageRef.put(file).then((resp) => {
        this.getImage();
      })
    }
  }


}
