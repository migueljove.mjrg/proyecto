import { Component } from '@angular/core';
import { LocalNotifications, ILocalNotification } from '@ionic-native/local-notifications/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  scheduled = [];

  constructor(
    private localNotification: LocalNotifications,
    private router: Router
  ) {
    this.getAllNotification();
  }

  getAllNotification() {
    this.localNotification.getAll().then((res: ILocalNotification[]) => {
      this.scheduled = res;
    })
  }

  cancelNotification(id) {
    this.localNotification.cancel(id);
  }

  doRefresh(event) {
    setTimeout(() => {
      this.localNotification.getAll().then((res: ILocalNotification[]) => {
        this.scheduled = res;
        event.target.complete();
      });
    }, 1000);
  }

  goToDetalle(n) {
    this.router.navigate(['/medicacion', n.id]);
  }
}
