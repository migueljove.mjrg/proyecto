import { Component, OnInit } from '@angular/core';
import { FirestoreService } from '../providers/firestore.service';
import { Medicacion } from '../providers/medicacion';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Location } from '@angular/common';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Platform, AlertController } from '@ionic/angular';
import { LocalNotifications, ELocalNotificationTriggerUnit } from '@ionic-native/local-notifications/ngx';

@Component({
  selector: 'app-add-medicacion',
  templateUrl: './add-medicacion.page.html',
  styleUrls: ['./add-medicacion.page.scss'],
})
export class AddMedicacionPage implements OnInit {

  public medicationForm: FormGroup;
  public name: string;
  public medicationsNames: any[];
  public filteredMedications: any[];
  private _medicationsNameURL = '../assets/convertcsv.json';
  private _medicationsURL = '../assets/medicamentos.json';

  constructor(
    private firestore: FirestoreService,
    public formBuilder: FormBuilder,
    private location: Location,
    private http: HttpClient,
    private plt: Platform,
    private localNotifications: LocalNotifications,
    private alertCtrl: AlertController
  ) {
    this.medicationForm = this.createMyForm();
  }


  ngOnInit() {
    this.getJSON(this._medicationsNameURL).subscribe(resp => this.medicationsNames = resp);
  }

  public getJSON(url): Observable<any> {
    return this.http.get(url);
  }

  filterMedication(event) {
    this.filteredMedications = [];
    for (let i = 0; i < this.medicationsNames.length; i++) {
      let medication = this.medicationsNames[i];
      if (medication.toLowerCase().indexOf(event.query.toLowerCase()) == 0) {
        this.filteredMedications.push(medication);
      }
    }
  }
  addMedicacion() {
    const dosis = this.medicationForm.controls['dosis'].value;
    const medida = this.medicationForm.controls['medida'].value;
    const tiempo_tomas = this.medicationForm.controls['tiempo_tomas'].value;
    const duracion_tratamiento = this.medicationForm.controls['duracion_tratamiento'].value;
    const comentario = this.medicationForm.controls['comentario'].value;
    let codigo_nacional = -1;
    this.getJSON(this._medicationsURL).subscribe(resp => {
      let lista = resp;
      for (let i = 0; i < lista.length; i++) {
        let medication = lista[i];
        if (medication.Medicamento.toLowerCase().indexOf(this.name.toLowerCase()) == 0) {
          codigo_nacional = medication.NRegistro;
        }
      }
      const med = new Medicacion(this.name, codigo_nacional, dosis, medida, tiempo_tomas, duracion_tratamiento, comentario);
      const texto = "Es la hora de tomar su medicacion. Ahora debe tomar " + med.dosis + " " + med.medida + " de " + med.name;

      this.localNotifications.schedule({
        id: med.codigo_nacional,
        title: med.name,
        text: texto,
        trigger: { every: ELocalNotificationTriggerUnit.MINUTE, count: med.tiempo_tomas }
      });
      this.firestore.addMedicacion(med);
      this.location.back();
    });
  }

  private createMyForm() {
    const group: FormGroup = new FormGroup({
      dosis: new FormControl('', Validators.required),
      medida: new FormControl('', Validators.required),
      tiempo_tomas: new FormControl('', Validators.required),
      duracion_tratamiento: new FormControl('', Validators.required),
      comentario: new FormControl('')
    });
    return group;
  }

  goBack() {
    this.location.back();
  }


}
