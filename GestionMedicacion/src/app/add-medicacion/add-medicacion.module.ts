import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { AddMedicacionPage } from './add-medicacion.page';
import { FormsModule } from '@angular/forms';
const routes: Routes = [
  {
    path: '',
    component: AddMedicacionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    AutoCompleteModule, 
  ],
  declarations: [AddMedicacionPage]
})
export class AddMedicacionPageModule { }
