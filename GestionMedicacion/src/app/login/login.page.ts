import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Validators, FormBuilder } from '@angular/forms';
import { AuthenticatorService } from '../providers/authenticator';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {
  userFormBuilder: any;

  constructor(
    public modalController: ModalController,
    private formBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private authenticator: AuthenticatorService,
    private router: Router,
    private storage: Storage
  ) {
    this.userFormBuilder = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.storage.get('uid').then((resp) => {
      if (resp !=='NoUID') {
        this.authenticator.uid = resp;
        this.router.navigateByUrl('/tabs/tab1');
      }
    });
  }

  // Perform login using user and password
  login() {
    const email = this.userFormBuilder.controls.email.value;
    const password = this.userFormBuilder.controls.password.value;
    this.authenticator.login(email, password)
      .then((user) => {
        this.authenticator.uid = user.user.uid;
        this.storage.set('uid', this.authenticator.uid);
        this.router.navigateByUrl('/tabs/tab1');
      })
      .catch((e) => {
        this.alertCtrl.create({
          header: 'Error',
          message: `Failed to login ${e.message}`,
          buttons: [{ text: 'Ok' }]
        }).then(alert => alert.present());
      });
  }

  // Push registration view
  signUp() {
    this.router.navigateByUrl('registration');
  }

  // Reset password
  resetPassword() {
    this.alertCtrl.create({
      header: 'Restablecer tu contraseña',
      message: 'Introduce tu email para que te podamos mandar un enlace para restablecer tu contraseña',
      inputs: [{ type: 'email', name: 'email', placeholder: 'Email' }],
      buttons: [
        { text: 'Cancelar', handler: () => { } },
        {
          text: 'Restablecer',
          handler: data => {
            this.authenticator.resetPassword(data.email)
              .then(() => {
                this.alertCtrl.create({
                  header: 'Aceptado',
                  message: 'Tu contraseña ha sido restablecida - Por favor comprueba tu email para seguir el resto de instrucciones.',
                  buttons: [{ text: 'Ok' }]
                }).then(alert => alert.present());
              })
              .catch((e) => {
                this.alertCtrl.create({
                  header: 'Error',
                  message: `Failed to login ${e.message}`,
                  buttons: [{ text: 'Ok' }]
                }).then(alert => alert.present());
              });
          }
        }
      ]
    }).then(alert => alert.present());
  }

}
