import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs';
import { User } from './user';
import { Medicacion } from './medicacion';
import { AuthenticatorService } from './authenticator';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  constructor(
    public firestore: AngularFirestore,
    public auth: AuthenticatorService, ) { }

  public getUserData(): Observable<any> {
    const uid = this.auth.uid;
    return this.firestore.collection('users').doc(uid).valueChanges();
  }

  public getMedications(): Observable<any> {
    const uid = this.auth.uid;
    return this.firestore.collection('userMedications').doc(uid).collection('medications').valueChanges();
  }

  public updateUserInfo(user: User) {
    this.firestore.collection('users').doc(user.uid).set(Object.assign({}, user));
  }

  public addMedicacion(medicacion: Medicacion) {
    const uid = this.auth.uid;
    this.firestore.collection('userMedications').doc(uid).collection('medications').doc(medicacion.codigo_nacional.toString()).set(Object.assign({}, medicacion));
  }

  public deleteMedicacion(medicacion: Medicacion) {
    const uid = this.auth.uid;
    this.firestore.collection('userMedications').doc(uid).collection('medications').doc(medicacion.codigo_nacional.toString()).delete().then(function () {
      console.log("Document successfully deleted!");
    }).catch(function (error) {
      console.error("Error removing document: ", error);
    });
  }


}
