import { Injectable } from '@angular/core';
import { Events } from '@ionic/angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireObject } from 'angularfire2/database';
import { Facebook } from '@ionic-native/facebook/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { TwitterConnect } from '@ionic-native/twitter-connect/ngx';
import { Loader } from './loader';
import { Config } from '../config';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { reject } from 'q';
import { FirestoreService } from './firestore.service';
import { AngularFirestore } from 'angularfire2/firestore';

@Injectable()
export class AuthenticatorService {

  public userRef: AngularFireObject<any>;
  public userDetails$: Observable<any>;
  public uid: string;

  constructor(
    private events: Events,
    private afAuth: AngularFireAuth,
    private loader: Loader,
    private router: Router,
    private firestore: AngularFirestore,
  ) { }

  // Perform login using user and password
  login(email: string, password: string) {
    var promise = new Promise<any>((resolve, reject) => {
      this.loader.show("Logging with Firebase email/password");
      this.afAuth.auth.signInWithEmailAndPassword(email, password)
        .then((user) => {
          this.loader.hide();
          this.events.publish('user:login', user);
          resolve(user);
        })
        .catch(e => {
          this.loader.hide();
          console.error(`Password Login Failure:`, e)
          reject(e);
        });
    });
    return promise;
  }

  // Reset password
  resetPassword(email) {
    const promise = new Promise<any>((resolve, reject) => {
      this.loader.show('Resetting your password');
      firebase.auth().sendPasswordResetEmail(email).
        then((result: any) => {
          this.loader.hide();
          this.events.publish('user:resetPassword', result);
          resolve();
        }).catch((e: any) => {
          this.loader.hide();
          reject(e);
        });
    });
    return promise;
  }


  logOut() {
    this.afAuth.auth.signOut().then((result: any) => {
      this.router.navigateByUrl('/login')
    }).catch((e: any) => {
      reject(e);
    });
  }

  deleteAccount() {
    this.firestore.collection('users').doc(this.uid).delete();
    this.firestore.collection('userMedications').doc(this.uid).delete();
    this.logOut();
  }
}
