import { TestBed } from '@angular/core/testing';

import { CimaRestApiService } from './cima-rest-api.service';

describe('CimaRestApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CimaRestApiService = TestBed.get(CimaRestApiService);
    expect(service).toBeTruthy();
  });
});
