export class Medicacion {
    name: string;
    codigo_nacional: number;
    dosis: number;
    medida: string;
    tiempo_tomas: number;
    duracion_tratamiento: number;
    comentario: string;
    constructor(
        name: string,
        codigo_nacional: number,
        dosis: number,
        medida: string,
        tiempo_tomas: number,
        duracion_tratamiento: number,
        comentario: string
    ) {
        this.name = name;
        this.codigo_nacional = codigo_nacional;
        this.dosis = dosis;
        this.medida = medida;
        this.tiempo_tomas = tiempo_tomas;
        this.duracion_tratamiento = duracion_tratamiento;
        this.comentario = comentario;
    }
}