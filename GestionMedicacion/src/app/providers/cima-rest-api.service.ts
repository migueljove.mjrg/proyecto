import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CimaRestApiService {

  urlBase: string = 'https://cima.aemps.es/cima/rest/medicamento?nregistro=';

  constructor(private http: HttpClient) { }

  getInfo(cod_nacional){
    const url = this.urlBase + cod_nacional;
    return this.http.get(url);
  }
}
