import { Medicacion } from './medicacion';

export class User {
  public fullName: string;
  public email: string;
  public avatar: string;
  public uid: string;

  constructor(snapshot) {
    this.uid = snapshot.uid;
    this.fullName = snapshot.fullName ? snapshot.fullName : 'Anonymous';
    this.email = snapshot.email;
    this.avatar = this.loadAvatar(snapshot.avatar);
  }

  // Verify if there is an avatar, if not assign a default one
  loadAvatar(avatarUrl) {
    return avatarUrl ? avatarUrl : 'assets/icon/no-avatar.png';
  }


}
