import { Injectable } from "@angular/core";
import * as firebase from 'firebase/app';
import 'firebase/storage';
import { AuthenticatorService } from './authenticator';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(private auth: AuthenticatorService) { }

  encodeImageUri(imageUri, callback) {
    var c = document.createElement('canvas');
    var ctx = c.getContext("2d");
    var img = new Image();
    img.onload = function () {
      var aux: any = this;
      c.width = aux.width;
      c.height = aux.height;
      ctx.drawImage(img, 0, 0);
      var dataURL = c.toDataURL("image/jpeg");
      callback(dataURL);
    };
    img.src = imageUri;
  };

  uploadImage(imageURI) {
    return new Promise<any>((resolve, reject) => {
      let storageRef = firebase.storage().ref();
      let imageRef = storageRef.child('images').child(this.auth.uid);
      this.encodeImageUri(imageURI, function (image64) {
        imageRef.putString(image64, 'data_url')
          .then(snapshot => {
            resolve(snapshot.downloadURL)
          }, err => {
            console.log(err);
            reject(err);
          })
      })
    })
  }

}
