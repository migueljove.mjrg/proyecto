import { Component } from '@angular/core';

import { Platform, Events } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';

import { filter, switchMap } from 'rxjs/operators';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import { AuthenticatorService } from './providers/authenticator';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  rootPage: any;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private events: Events,
    private auth: AngularFireAuth,
    private db: AngularFireDatabase,
    private authenticatorService: AuthenticatorService,
    private router: Router,
    private backgroundMode: BackgroundMode
  ) {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.backgroundMode.enable();
      router.navigateByUrl('/login');
    });
  }

  signOut() {
    this.auth.auth.signOut();
  }

  initializeApp() {
    //Verify if user is logged in
    this.auth.authState.subscribe(oAuthData => {
      oAuthData ? this.router.navigateByUrl('/tabs') : this.router.navigateByUrl('/login');
    });

    // Load use details into service object
    this.authenticatorService.userDetails$ = this.auth.authState.pipe(
      filter(oAuthData => !!oAuthData && !!oAuthData.uid),
      switchMap(oAuthData => {
        this.authenticatorService.userRef = this.db.object('users/' + oAuthData.uid);
        return this.authenticatorService.userRef.valueChanges();
      }),
      filter(userDetails => !!userDetails)
    );

    this.events.subscribe('user:login', eventData => {
      const user = eventData.user || eventData;
      const userRef = this.db.object(`users/${user.uid}`);

      const dataToPersist = {};
      this.authenticatorService.uid = user.uid;
      dataToPersist['provider'] = user.isAnonymous ? 'anonymous' : user.providerData[0].providerId;
      dataToPersist['uid'] = user.uid;
      dataToPersist['avatar'] = user.photoURL || 'assets/icon/no-avatar.png';

      dataToPersist['fullName'] = user.displayName || 'Anonymous';
      dataToPersist['email'] = user.email;

      userRef.set(dataToPersist);
    });

    this.events.subscribe('user:create', eventData => {
    });

    this.events.subscribe('user:resetPassword', eventData => {
    });
  }
}
