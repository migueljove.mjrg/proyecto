import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'registration', loadChildren: './registration/registration.module#RegistrationPageModule' },
  { path: 'tabs', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'medicacion', loadChildren: './add-medicacion/add-medicacion.module#AddMedicacionPageModule' },
  { path: 'farmacias-cercanas', loadChildren: './farmacias-cercanas/farmacias-cercanas.module#FarmaciasCercanasPageModule' },
  { path: 'medicacion/:id', loadChildren: './medicacion/medicacion.module#MedicacionPageModule' },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
