import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { OverlayEventDetail } from '@ionic/core';
import { FirestoreService } from '../providers/firestore.service';
import { Medicacion } from '../providers/medicacion';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  medications: Array<Medicacion>;

  constructor(private firestore: FirestoreService,
    private router: Router) {
  }

  ngOnInit(): void {
    this.firestore.getMedications().subscribe(resp => {
      this.medications = resp;
    });
  }

  goToDetalle(codigo_nacional) {
    this.router.navigate(['/medicacion', codigo_nacional]);
  }
}
