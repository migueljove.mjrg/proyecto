import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmaciasCercanasPage } from './farmacias-cercanas.page';

describe('FarmaciasCercanasPage', () => {
  let component: FarmaciasCercanasPage;
  let fixture: ComponentFixture<FarmaciasCercanasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmaciasCercanasPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmaciasCercanasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
