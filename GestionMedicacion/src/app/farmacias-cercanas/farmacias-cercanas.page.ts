import { Component, OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx'
import { LoadingController } from '@ionic/angular'
import { Location } from '@angular/common';

declare var google;

@Component({
  selector: 'app-farmacias-cercanas',
  templateUrl: './farmacias-cercanas.page.html',
  styleUrls: ['./farmacias-cercanas.page.scss'],
})
export class FarmaciasCercanasPage implements OnInit {

  constructor(
    private geolocation: Geolocation,
    private loadCtrl: LoadingController,
    private location: Location,
  ) { }

  ngOnInit() {
    this.loadMap();
  }

  goBack() {
    this.location.back();
  }

  async loadMap() {
    const loading = await this.loadCtrl.create();
    loading.present();
    const rta = await this.geolocation.getCurrentPosition();
    const myLatLng = {
      lat: rta.coords.latitude,
      lng: rta.coords.longitude,
    };
    const mapEle: HTMLElement = document.getElementById('map');
    const infowindow = new google.maps.InfoWindow();
    const map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 13
    });
    google.maps.event.addListenerOnce(map, 'idle', () => {
      this.loadCtrl.dismiss();
      var request = {
        location: myLatLng,
        radius: '1200',
        type: ['pharmacy']
      };
      const service = new google.maps.places.PlacesService(map);
      service.nearbySearch(request, callback);
    });

    function callback(results, status) {
      if (status == google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; i < results.length; i++) {
          var place = results[i];
          createMarker(results[i]);
        }
      }
    }

    function createMarker(place) {
      var marker = new google.maps.Marker({
        map: map,
        position: place.geometry.location
      });
      google.maps.event.addListener(marker, 'click', function () {
        var content = '<div id="content">' +
          '<div id="siteNotice">' +
          '</div>' +
          '<h1 id="firstHeading" class="firstHeading">' + place.name + '</h1>' +
          '<div id="bodyContent">' + '<p><b>' + place.vicinity + '</b></p>' +
          '</div>' +
          '</div>';
        infowindow.setContent(content);
        infowindow.open(map, this);
      });
    }
  }
}

