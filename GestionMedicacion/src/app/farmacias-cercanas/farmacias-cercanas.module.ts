import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { GMapModule } from 'primeng/gmap';
import { IonicModule } from '@ionic/angular';


import { FarmaciasCercanasPage } from './farmacias-cercanas.page';

const routes: Routes = [
  {
    path: '',
    component: FarmaciasCercanasPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    GMapModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FarmaciasCercanasPage]
})
export class FarmaciasCercanasPageModule { }
